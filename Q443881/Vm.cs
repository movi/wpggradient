using System.Collections.Generic;
using System.Collections.ObjectModel;
using DevExpress.Mvvm;

namespace Gradient
{
    public class Vm : ViewModelBase
    {
        private double _value;

        public Vm()
        {
            GridItems = new ObservableCollection<Item>(CreateData(10));
            ValueChangingCommand = new DelegateCommand<double>(ValueChangingCore);

            //�� �������� ������� � �������
            // https://www.devexpress.com/Support/Center/Question/Details/T215491
            //�������� ���
            //��������� ������ ��� �����
            //  devexpress event to command celltemplate
        }

        public DelegateCommand<double> ValueChangingCommand { get; set; }

        void ValueChangingCore(double o)
        {
            
        }

        // Dependency Property

        //public static readonly DependencyProperty CurrentTimeProperty =
        //    DependencyProperty.Register("CurrentTime", typeof(DateTime),
        //        typeof(MainWindow), new FrameworkPropertyMetadata(DateTime.Now));

        //// .NET Property wrapper
        //public DateTime CurrentTime
        //{
        //    get { return (DateTime) GetValue(CurrentTimeProperty); }
        //    set { SetValue(CurrentTimeProperty, value); }
        //}

        public ObservableCollection<Item> GridItems
        {
            get;
            set;
        }

        public double Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                RaisePropertyChanged("Value");
            }
        }

        private  List<Item> CreateData(int length)
        {
            List<Item> list = new List<Item>();
            for (int i = 0; i < length; i++)
            {
                var item = new Item();
                item.Value = i;
                item.MinAvailableValue = 0.1;
                list.Add(item);
            }
            return list;
        }
    }
}