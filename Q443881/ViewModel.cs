using DevExpress.Mvvm;

namespace Gradient
{
    public class ViewModel : ViewModelBase
    {
        private double _value;

        public ViewModel()
        {
            //TODO: trackBarEdit1_EditValueChanging to command
            ValueChangingCommand = new DelegateCommand<double>(ValueChangingCore);
        }

        public DelegateCommand<double> ValueChangingCommand { get; set; }

        void ValueChangingCore(double o)
        {

        }

        public double Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                RaisePropertyChanged();
            }
        }

        public double Val
        {
            get { return Value/100d; }
        }
    }
}
