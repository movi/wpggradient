﻿using System.ComponentModel;

namespace Gradient
{
    public class Item : INotifyPropertyChanged
    {
        public double Val
        {
            get { return _value/100; }
        }

        public double Value
        {
            get { return _value; }
            set
            {
                _value = value; 
             RaisePropertyChanged("Value");
             RaisePropertyChanged("Val"); 
            }
        }

        public double MinAvailableValue
        {
            get { return _minAvailableValue; }
            set { _minAvailableValue = value; }
        }

        private bool _isChecked;
        private double _value;
        private double _minAvailableValue;

        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                if (_isChecked == value)
                    return;
                _isChecked = value;
                RaisePropertyChanged("Boolean");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged(string name)
        {
            var handler = PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}