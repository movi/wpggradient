﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Editors;

namespace Gradient.EventConverters
{
    using System.Linq;
    public class TrackBarEventArgsConverter : EventArgsConverterBase<EditValueChangingEventArgs>
    {
        protected override object Convert(object sender, EditValueChangingEventArgs e)
        {
            TrackBarEdit trackBar = (TrackBarEdit)sender;
            
            //TODO:использование значения типа в ячейке, а не значения trackbar
            if (System.Convert.ToInt32(e.NewValue) < 1)
                //GetMinAvailableValue(trackBar) || Convert.ToInt32(e.NewValue) > GetMaxAvailableValue(trackBar))
            {
                e.IsCancel = true;
                e.Handled = true;
            }

            //ListBox parentElement = (ListBox)sender;
            //DependencyObject clickedElement = (DependencyObject)args.OriginalSource;
            //ListBoxItem clickedListBoxItem =
            //    LayoutTreeHelper.GetVisualParents(child: clickedElement, stopNode: parentElement)
            //    .OfType<ListBoxItem>()
            //    .FirstOrDefault();
            //if (clickedListBoxItem != null)
            //    return (Person)clickedListBoxItem.DataContext;
            return null;
        }
    }
}
